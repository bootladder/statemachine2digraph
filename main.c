#include <stdio.h>
#include <stdlib.h>

#include "LineMatchDispatch.h"
#include "States.h"

static char linebuf[256];
FILE * sourcefile;

int main(int argc, char** argv)
{
  LineMatcher_Init(argv[2],argv[3]);
  //Open the file
  sourcefile = fopen(argv[1],"r"); 
  if( sourcefile == NULL ) {
    perror("Fail... fopen(sourcefilename): ");
    exit(1);
  }

  //For Every line in the source file
  while(fgets(linebuf, 256, sourcefile) != NULL)
  {
    LineMatchDispatch(linebuf);
  } 

  //Output the digraph
  printf("digraph{\n");
  States_PrintDigraph();
  printf("}\n");
  (void)argc;
}

