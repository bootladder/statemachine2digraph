#!/bin/bash
mkdir -p output

echo compiling gcc
gcc -Wextra -Wall -Werror main.c LineMatchDispatch.c States.c && \
echo compiling g++
g++ -Wextra -Wall -Werror main.c LineMatchDispatch.c States.c && \

  echo RUNNING ALARM && \
  ./a.out scratch/Alarm* alarmSubState ALARM_SUBSTATE > temp
  dot -Tpng -O temp 
  mv temp.png alarm.png

  echo RUNNING MESSAGES && \
  ./a.out scratch/NoAlarmH* State_NoAlarmHandleMessages NOALARM_SUBSTATE > temp
  dot -Tpng -O temp 
  mv temp.png noalarmhandlemessages.png

  echo RUNNING VALVE1 && \
  ./a.out scratch/Valve1* Valve1State VALVE_STATE > temp
  dot -Tpng -O temp 
  mv temp.png valveapp.png

  echo RUNNING NONETWORK && \
  ./a.out scratch/NoNet* noNetworkSubState NONETWORK_SUBSTATE > temp
  dot -Tpng -O temp 
  mv temp.png nonetwork.png

  rm temp
