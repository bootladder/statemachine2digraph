#include "digraph.h"

char * edges[100];
int edgeIndex = 0;

/*take 2 strings, make it look like
  a -> b;
*/
int digraph_AddEdge(char * a, char *b)
{
  int size_a = strlen(a);
  int size_b = strlen(b);

  char * newEdge = malloc(size_a+size_b+50);
  newEdge[0] = '\0';
  strcat(newEdge,a);
  strcat(newEdge," -> ");
  strcat(newEdge,b);
  edges[edgeIndex]=newEdge; 
  if(++edgeIndex==100)
    edgeIndex=0;
}

