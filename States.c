#include "States.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#define BUFSIZE 64
typedef struct {
  char newstate[BUFSIZE];
  char condition[BUFSIZE];
} Transitions_t;

typedef struct {
  char name[BUFSIZE];
  Transitions_t transitions[BUFSIZE];
} State_t;

State_t States[BUFSIZE] = {};
int stateindex = -1;
int transitionindex = 0;


int States_AddState(char * newstate)
{
  stateindex++;
  transitionindex = 0;
  memcpy(States[stateindex].name, newstate,BUFSIZE);
  return 0;
}


int States_AddTransition(char * newstate, char * condition)
{
  memcpy(States[stateindex].transitions[transitionindex].newstate,newstate,BUFSIZE);
  transitionindex++;
  (void)condition;
  return 0;
}

void States_PrintDigraph(void)
{
  for(int i=0;i<BUFSIZE;i++){
    char * thisname = States[i].name;
    if(thisname){
      for(int z=0;z<BUFSIZE;z++){
        char * nextstate = States[i].transitions[z].newstate;
        if( *nextstate ){
          printf("%s -> %s\n",thisname,nextstate);
        }
      }

    }
  }
}
