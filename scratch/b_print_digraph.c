#include <stdio.h>
#include <malloc.h>
#include <string.h>

char * edges[100];
int edgeIndex = 0;
char linebuf[80];
FILE * sourcefile;
char sourcefilename[100]="/tmp/source";
char statevariable[100] = "noAlarmSubState";
char stateprefix[100] = "NOALARM_SUBSTATE";

/*take 2 strings, make it look like
  a -> b;
*/
int digraph_AddEdge(char * a, char *b)
{
  int size_a = strlen(a);
  int size_b = strlen(b);

  char * newEdge = malloc(size_a+size_b+50);
  newEdge[0] = '\0';
  strcat(newEdge,a);
  strcat(newEdge," -> ");
  strcat(newEdge,b);
  edges[edgeIndex]=newEdge; 
  if(++edgeIndex==100)
    edgeIndex=0;
}

char lastSrc[100];
char lastDst[100];
char * x;

int main(void)
{
  printf("digraph{\n");
  sourcefile = fopen(sourcefilename,"r"); 
  //Every line in the source file
  while(fgets(linebuf, 80, sourcefile) != NULL)
  {
    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    #define CONTAINS(s,n) strstr(s,n) != NULL
    #define INDEXOF(s,n) strstr(s,n)

    if(linebuf[0] == '/') //skip comments
      continue;

    //Look for a switch case.  If found, store the state
    //char * x = strstr(linebuf,"case");
    //if( x != NULL)
    //memcpy(lastSrc,linebuf,80);
    if( CONTAINS(linebuf,"case") )
    {
      char * end = INDEXOF(linebuf,":");  //strchr(linebuf,':');
      *end = '\0';
      x = INDEXOF(linebuf,stateprefix); //strstr(linebuf,stateprefix);
      memcpy(lastSrc,x,80);
      continue;
    }

    //Look for a state variable assignment
    //statevariable = STATE;
    //x = strstr(linebuf,statevariable);
    //if( x != NULL)
    if( CONTAINS(linebuf,statevariable) )
    {
      x = INDEXOF(linebuf,stateprefix); //strstr(linebuf,stateprefix);
      //strip the semicolon char * end = strchr(linebuf,';');
      if( x != NULL )
      {
        *end = '\0';
        //verify there is also a state prefix, 
        memcpy(lastDst,x,80);
        printf("%s -> %s\n",lastSrc,x);
      }
    } 
    //printf("%s\n",linebuf);
    fflush(stdout);
  } 
  printf("}\n");
}


/*

  digraph_AddEdge("blah","arrr");
  digraph_AddEdge("blah","arrr");
  digraph_AddEdge("durr","123");
  for(int i=0;i<3;i++)
    printf("edges[%d] : %s\n",i,edges[i]);
*/
