
StateMachine_State_t ValveTransitionTable[] = {
	
	[VALVE_STATE_INIT] = {

		.Do_Onces={MACHINE_D_NOTHING},
		.Conditions={MACHINE_C_ALWAYS,VALVE_STATE_CHECK_CONNECTED_VALVES}
	}
	,
	[VALVE_STATE_CHECK_CONNECTED_VALVES] = {
		.Do_Onces={D_Valve_Enable,D_Valve_Close},
		.Conditions={MACHINE_C_ALWAYS,VALVE_STATE_WAIT_CHECK_TIMER}
	}
	,
	[VALVE_STATE_WAIT_CHECK_TIMER] = {
		.Do_Onces={MACHINE_D_NOTHING},
		.Conditions={
			{C_Valve_IsClosed,VALVE_STATE_DEBOUNCE1},
			{C_Valve_IsTimeout,VALVE_STATE_DISCONNECTED}
		}
	}
	,
	[VALVE_STATE_DEBOUNCE1] = {
		.Do_Onces={D_Valve_Debounce_Start},
		.Conditions={C_Valve_IsDebounced,VALVE_STATE_IDLE}
	}
	,
	[VALVE_STATE_IDLE] = {
		.Do_Onces={MACHINE_D_NOTHING},
		.Conditions={
			{C_Valve_IsMoving,VALVE_STATE_MOVING},
		}
	}
	,
	[VALVE_STATE_DISCONNECTED] = {
		.Do_Onces={MACHINE_D_NOTHING},
		.Conditions={
			{C_Valve_IsConnected,VALVE_STATE_IDLE},
		}
	}
};

