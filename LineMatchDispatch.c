#include "LineMatchDispatch.h"
#include "digraph.h"
#include "States.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define HAS_SUBSTRING(s,n) (strstr(s,n) != NULL)
#define INDEXOF(s,n) strstr(s,n)
#define ASSERT(a) if(!a){printf("FAIL_ASSERT");exit(1);}

static int is_comment(char * buf);
static bool LineMatcher_IsSwitchCase(char * line);
static bool LineMatcher_IsStateVariableAssignment(char * line);
static char * get_statename_from_switchcase(char * buf);
static char * get_newstate_from_variableassignment(char * buf);
static bool CONTAINS(const char * s, const char * n);
static void check_switch_reached(char * line);

char sourcefilename[] = "/tmp/source";
const char * statevariable  = "noAlarmSubState";
const char * stateprefix    = "NOALARM_SUBSTATE_";

char lastCase[100];
char lastDst[100];

bool isSwitchReached = false; //skip lines before the switch

void LineMatcher_Init(char * s, char * p)
{
  statevariable = s;
  stateprefix = p;
}

int LineMatchDispatch(char * line)
{
    //Debug:  Print every Line
    //printf("%s\n",line);

    check_switch_reached(line);
    if( false == isSwitchReached )
      return 1;

    if( is_comment(line) ) //skip comments
      return 1;

    if( LineMatcher_IsSwitchCase(line) ) {
      //printf("save_case_state:  %s",line);
      char * newcase = get_statename_from_switchcase(line);
      States_AddState(newcase);
      return 1;
    }

    if( LineMatcher_IsStateVariableAssignment(line) ) {
      //printf("save_state_transition:  %s",line);
      char * newcase = get_newstate_from_variableassignment(line);
      States_AddTransition(newcase,(char*)"");
      return 1;
    }
    return 0;
}

static void check_switch_reached(char * line)
{
   if( CONTAINS(line,"switch") )
    isSwitchReached = true; 
}

static int is_comment(char * buf)
{
    int i;
    for(i=0;isspace(buf[i]);i++);

    if(buf[i] == '/')
        return 1;
    return 0;
}

static bool LineMatcher_IsSwitchCase(char * line) {
    return CONTAINS(line,"case") &&
           CONTAINS(line,":") &&
           CONTAINS(line,stateprefix) 
      ;
}

//take a string like "case STATE_BLAH:" , store "STATE_BLAH"
static char * get_statename_from_switchcase(char * buf)
{
    char * start;
    char * end;
    if( false == HAS_SUBSTRING(buf,":") )         return NULL;
    if( false == HAS_SUBSTRING(buf,stateprefix) ) return NULL;

    start = INDEXOF(buf,stateprefix);
    end = INDEXOF(buf,":");
    *end = '\0';
    memcpy(lastCase,start,80);
    return lastCase;
}

static char * get_newstate_from_variableassignment(char * buf)
{
      //strip the semicolon 
      char * end = strchr(buf,';');
      *end = '\0';
      //find the new state
      char * x = INDEXOF(buf,stateprefix);
      memcpy(lastDst,x,80);

      return lastDst;
}


static bool LineMatcher_IsStateVariableAssignment(char * line) {
  return CONTAINS(line,statevariable) &&
         CONTAINS(line,stateprefix)   &&
         CONTAINS(line,";")   &&
         CONTAINS(line,"=")   ;
    //  )

}

static bool CONTAINS(const char * s, const char * n)
{
  ASSERT(s != 0 && n != 0)
  const char * c = strstr(s,n);
  if(c == NULL){
    return false;
  }
  return true;
}

